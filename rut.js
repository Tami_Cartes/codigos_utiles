function limpiarRut(rutstring) {
  /**
   * Funcion hecha para devolver el rut separado en dos partes.
   * El numero de la izquierda, sin digito verificador.
   * Y el digito verificador aislado 
   */
  var izquierda, verificador, tmp;
  var reg = /[^0-9Kk]/; //Regex = match con algo que no sea numerico, o alguna letra K
  tmp = rutstring;
  if (tmp) {
    while (reg.test(tmp)) {
      tmp = tmp.replace(reg, "")
    }
    izquierda = tmp.substring(0, tmp.length - 1);
    verificador = tmp.substring(tmp.length - 1, tmp.length);
    // Si el digito verificador es letra, lo devuelvo en minusculas
    if (typeof verificador === 'string') {
      verificador = verificador.toUpperCase()
    }
  }
  return { izquierda, verificador };
}

function digitoVerificador(rutNoDV) {
  /**
   * Funcion pensada para calcular el digito verificador de un rut.
   * retorna el numero verificador.
   * 
   * Pensado para trabajar solo con la parte izquierda del rut
   */
  var suma = 0;
  var tmp = Number(rutNoDV);
  var magicNums = [2, 3, 4, 5, 6, 7];
  while (tmp > 0) {
    var digit = tmp % 10;
    tmp = Math.trunc(tmp / 10);
    suma += digit * magicNums[0];
    magicNums.push(magicNums.shift());
  }
  var dv = 11 - (suma % 11);

  return dv
}
function verificar(rutString) {
/**
 * Funcion pensada para utilizarse con la parte izquierda y el digito verificador
 */
  var validacion = 0
  var { izquierda, verificador } = limpiarRut(rutString)
  for(var i = 0; i<rutString.length;i++ ){
      rutString=rutString.replace(".",""); 
  } 
  $("#SOMEID").val(rutString.toUpperCase())
  console.log(rutString)
  if (!izquierda || !verificador) return false
  var miDV = digitoVerificador(izquierda);
  if (miDV == 11) {
    var r = rutString.substr(rutString.length-2,1)
    console.log('r '+r)
    if(r == "-"){
    validacion = 2
        //HACER ALGO ACA       
    }else{
      alert("El rut debe tener formato 12345678-K")    
      }
    miDV = 0;
  } else if (miDV == 10) {
    var r = rutString.substr(rutString.length-2,1)
    console.log('r'+r)
    if(r == "-"){
    validacion = 2
        //HACER ALGO ACA
       
    }else{
      alert("El rut debe tener formato 12345678-K")
      }
    miDV = 'K';
  }else{
      if (miDV == verificador) {
    var r = rutString.substr(rutString.length-2,1)
      console.log('r '+r)
        if(r == "-"){
          validacion = 2
          //HACER ALGO ACA
        }
    return true
    }else{
      alert("El RUT no es válido,  debe tener formato 12345678-K")
      }
}
  
return false
}